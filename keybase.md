### Keybase proof

I hereby claim:

  * I am voided on github.
  * I am voided (https://keybase.io/voided) on keybase.
  * I have a public key whose fingerprint is 0412 5B38 95BC 98BB FD85  2429 4609 49AA 1EB8 7798

To claim this, I am signing this object:

```json
{
  "body": {
    "key": {
      "eldest_kid": "01016c2f1a3b54decec257d0914f6f0aac9d661a36bc910c30b24c82fbd5fbe381950a",
      "fingerprint": "04125b3895bc98bbfd852429460949aa1eb87798",
      "host": "keybase.io",
      "key_id": "460949aa1eb87798",
      "kid": "01016c2f1a3b54decec257d0914f6f0aac9d661a36bc910c30b24c82fbd5fbe381950a",
      "uid": "df6a068ab74af34949387b4ebffe6b19",
      "username": "voided"
    },
    "service": {
      "name": "github",
      "username": "voided"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "ctime": 1532799174,
  "expire_in": 157680000,
  "prev": "347574555f37922412d0a12c09bc132dcc055b65b29c76e1b3953ff7f95e8951",
  "seqno": 2,
  "tag": "signature"
}
```

with the key [0412 5B38 95BC 98BB FD85  2429 4609 49AA 1EB8 7798](https://keybase.io/voided), yielding the signature:

```
-----BEGIN PGP MESSAGE-----
Version: Keybase OpenPGP v2.0.77
Comment: https://keybase.io/crypto

yMISAnicrVJfSBRBHD4zE62kMBCPoFhBSax2dnd2d9QgE/TBp4y0UDt2dmfPTb27
7p8eJl3k09kFRkFlIKFRDykSvpSFd2adYmZSWRCJxVUioaaQCCXNir3VW/MyzPf7
vm+++c1veHuiJTUhUtHBHnsfPZ8wFm3zWaqq7z5rZrBTCzD5zUwdWd9IvUY8Xlud
oTH5DAtYIKqcDhQeQ0EjKlE5KGksAoIu6qyiqEgTRVoVsYoAq/Is5gRV5nSsQR0T
XgYIsgqTx+iGw07cLrfh8Jq2AuAg5mUEqUzGWNdkyAkcEkQWCUhRAMGyJCGZCmud
HlNBw2HFQw4YTorRg2093l/4/zm3b91O00WFFWUFS4Ki8wK9k5clLBCs60TEAJlE
D3E7lAZC2X6noRGNacljKOY3VGK2daNmN7y1PvwvvjfgMoFGgm0bUhs2HBrtHVX4
idtjOB1MPqBM1WuYWgB5TkIISEIeQ5pchpvYDJMBJVFm6cpjXG7ip5a8IEFJgBDq
vIQ4jvZfYxXAqSzCKuA5TVVZCLEIMYdUSSQA8wjyui7pCBL6TYAxH3PG4WTyORpT
sVNLj2F3KF6fmzAtQ5HqzZaEVMuWpE3mTFlSU3b8mbTm18lrhWODs3GhPTzQujQ6
CpeXhZKBDwv6yOLEwcxV7JWXnnfU8CuhV+Rw+JK/cnKxe+jIowuDP1ZKQ4W5J3YD
wXY5o3OxPOtp3574lcz2Q6H6h76XOY29x0fZb9LOXydPi5+vts0VLRQU9ETLbWXa
RGs89sRaNv2p7n44ZNxo6rqVnns0fbjT0ueaDlXE5pJ6X7SeuidG07ms2Zy0cx8n
rO/m07Y+Tk4MB223Vx3K5M+LmZXB8WxuLTzV8L27YKrkbG5gvqDjy1xPLN63y/nm
+l7rvpm3LmMh0r+/M3cmu6jLVXXtTspYTbBr/GYDUxwIxDK29QeKH1i/jlRHgqXO
33rERVM=
=I31t
-----END PGP MESSAGE-----

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/voided

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id voided
```
